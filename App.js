/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import Landing from './src/pages/landing';
import Settings from './src/pages/settings';
import Videos from './src/pages/videos';
import Search from './src/components/searchHeader';
import Favorites from './src/pages/favorites';
import SelectedItems from './src/pages/selectedItems';
import HeaderItems from './src/components/headerItems';
import BackArrow from './src/components/backArrow';

const Tab = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();

function LandingStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Landing"
        component={Landing}
        options={{
          header: () => <Search />,
        }}
      />
      <Stack.Screen
        name="SelectedItem"
        options={{
          headerTitle: null,
          headerRight: () => <HeaderItems />,
          headerBackImage: () => <BackArrow />,
        }}
        component={SelectedItems}
      />
    </Stack.Navigator>
  );
}
function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName="Home"
        backgroundColor="#5cccee"
        activeColor="white"
        inactiveColor="white">
        <Tab.Screen
          name="Home"
          component={LandingStack}
          options={{
            tabBarLabel: 'Home',
            tabBarColor: '#5cccee',

            tabBarIcon: ({color}) => (
              <Icon name="home-outline" color={color} size={26} />
            ),
          }}
        />
        <Tab.Screen
          name="Videos"
          component={Videos}
          options={{
            tabBarLabel: 'Videos',
            tabBarColor: '#5cccee',

            tabBarAccessibilityLabel: 'Videos tab',
            tabBarIcon: ({color}) => (
              <Icon name="play-box-outline" color={color} size={26} />
            ),
          }}
        />

        <Tab.Screen
          name="Favorites"
          component={Favorites}
          options={{
            tabBarColor: '#5cccee',
            tabBarIcon: ({color}) => (
              <Icon name="heart-outline" color={color} size={26} />
            ),
          }}
        />
        <Tab.Screen
          name="Settings"
          component={Settings}
          options={{
            tabBarLabel: 'Settings',
            tabBarColor: '#5cccee',

            tabBarIcon: ({color}) => (
              <Icon name="settings-outline" color={color} size={26} />
            ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

export default App;
