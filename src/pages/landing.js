import React, {useState} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
} from 'react-native';
import {Paragraph, Caption} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
const {width, height} = Dimensions.get('window');
function Landing({navigation}) {
  return (
    <ScrollView>
      <View>
        <TouchableOpacity style={{height: 250}}>
          <Image
            style={styles.topImage}
            source={require('../assets/images/andy-hay--c-KHajV6kE-unsplash.jpg')}
          />
        </TouchableOpacity>
        <View style={{flexDirection: 'row'}}>
          <Paragraph style={styles.paragraph}>Trending</Paragraph>
          <Icon name="fire" color="red" size={18} />
        </View>
        <View>
          {console.log('hq')}
          <ScrollView
            horizontal
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{}}>
            <TouchableOpacity
              style={{paddingRight: 10}}
              onPress={() => {
                navigation.navigate('SelectedItem');
              }}>
              <Image
                source={require('../assets/images/anthony-ievlev-Y1hBfh_IKgI-unsplash.jpg')}
                style={{height: 100, width: 100}}
              />
              <Caption style={{fontSize: 18, paddingTop: 0}}>
                Tasty Cookies
              </Caption>
            </TouchableOpacity>
            <TouchableOpacity style={{paddingRight: 10}}>
              <Image
                source={require('../assets/images/anthony-ievlev-Y1hBfh_IKgI-unsplash.jpg')}
                style={{height: 100, width: 100}}
              />
              <Caption style={{fontSize: 18, paddingTop: 0}}>
                Tasty Cookies
              </Caption>
            </TouchableOpacity>
            <TouchableOpacity style={{paddingRight: 10}}>
              <Image
                source={require('../assets/images/anthony-ievlev-Y1hBfh_IKgI-unsplash.jpg')}
                style={{height: 100, width: 100}}
              />
              <Caption style={{fontSize: 18, paddingTop: 0}}>
                Tasty Cookies
              </Caption>
            </TouchableOpacity>
            <TouchableOpacity style={{paddingRight: 10}}>
              <Image
                source={require('../assets/images/anthony-ievlev-Y1hBfh_IKgI-unsplash.jpg')}
                style={{height: 100, width: 100}}
              />
              <Caption style={{fontSize: 18, paddingTop: 0}}>
                Tasty Cookies
              </Caption>
            </TouchableOpacity>
            <TouchableOpacity style={{paddingRight: 10}}>
              <Image
                source={require('../assets/images/anthony-ievlev-Y1hBfh_IKgI-unsplash.jpg')}
                style={{height: 100, width: 100}}
              />
            </TouchableOpacity>
            <TouchableOpacity style={{paddingRight: 10}}>
              <Image
                source={require('../assets/images/anthony-ievlev-Y1hBfh_IKgI-unsplash.jpg')}
                style={{height: 100, width: 100}}
              />
            </TouchableOpacity>
          </ScrollView>
        </View>
        <View>
          <ScrollView
            horizontal
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{}}>
            <TouchableOpacity style={{paddingRight: 10}}>
              <Image
                source={require('../assets/images/anthony-ievlev-Y1hBfh_IKgI-unsplash.jpg')}
                style={{height: 100, width: 100}}
              />
              <Caption style={{fontSize: 18, paddingTop: 0}}>
                Tasty Cookies
              </Caption>
            </TouchableOpacity>
            <TouchableOpacity style={{paddingRight: 10}}>
              <Image
                source={require('../assets/images/anthony-ievlev-Y1hBfh_IKgI-unsplash.jpg')}
                style={{height: 100, width: 100}}
              />
            </TouchableOpacity>
            <TouchableOpacity style={{paddingRight: 10}}>
              <Image
                source={require('../assets/images/anthony-ievlev-Y1hBfh_IKgI-unsplash.jpg')}
                style={{height: 100, width: 100}}
              />
            </TouchableOpacity>
            <TouchableOpacity style={{paddingRight: 10}}>
              <Image
                source={require('../assets/images/anthony-ievlev-Y1hBfh_IKgI-unsplash.jpg')}
                style={{height: 100, width: 100}}
              />
            </TouchableOpacity>
            <TouchableOpacity style={{paddingRight: 10}}>
              <Image
                source={require('../assets/images/anthony-ievlev-Y1hBfh_IKgI-unsplash.jpg')}
                style={{height: 100, width: 100}}
              />
            </TouchableOpacity>
            <TouchableOpacity style={{paddingRight: 10}}>
              <Image
                source={require('../assets/images/anthony-ievlev-Y1hBfh_IKgI-unsplash.jpg')}
                style={{height: 100, width: 100}}
              />
            </TouchableOpacity>
          </ScrollView>
        </View>
      </View>
    </ScrollView>
  );
}
export default Landing;

const styles = StyleSheet.create({
  paragraph: {
    color: 'red',
    opacity: 0.6,
    fontWeight: 'bold',
    fontSize: 20,
  },
  topImage: {width: width, height: '100%'},
});
