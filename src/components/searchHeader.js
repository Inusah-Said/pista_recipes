import React, {useState} from 'react';
import {Searchbar} from 'react-native-paper';

const Search = () => {
  const [searchText, setSearchText] = useState('');
  return (
    <Searchbar
      value={searchText}
      onChangeText={(text) => {
        setSearchText(text);
      }}
      selectionColor="#5cccee"
      style={{height: 50, opacity: 0.9, color: '#5cccee'}}
      placeholder="Search for Recipes"
      clearButtonMode="while-editing"
    />
  );
};

export default Search;
