import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const HeaderItems = () => {
  const [color, setcolor] = useState('#5cccee');
  const [name, setname] = useState('heart-outline');
  return (
    <View style={{flexDirection: 'row', paddingRight: 10}}>
      <Icon
        name="share-variant"
        style={{paddingRight: 20}}
        size={30}
        color="#5cccee"
        onPress={() => alert('hey there')}
      />
      <Icon
        name={name}
        size={30}
        color={color}
        style={{paddingRight: 10}}
        onPress={() => {
          if (color == '#5cccee' && name == 'heart-outline') {
            setcolor('red');
            setname('heart');
          } else {
            setcolor('#5cccee');
            setname('heart-outline');
          }
        }}
      />
    </View>
  );
};

export default HeaderItems;
