import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const BackArrow = () => {
  return <Icon name="arrow-left" color="#5cccee" size={30} />;
};

export default BackArrow;
